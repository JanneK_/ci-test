const chai = require("chai");
const expect = chai.expect;
const request = require("request");
const app = require("../src/server.js");
const port = 7373;

let server;

const a_request = async (value) => new Promise((resolve, reject) => {
    request(value, (error, response) => {
        if (error) reject(error);
        else resolve(response);
    });
});

describe("Test REST API", () => {
    beforeEach("Start server", async () => {
        server = app.listen(port);
    });

    describe("Test functionality", () => {        
        it("GET / returns 200, which means OK", async () => {
            // todo implement test
            const options = {
                method: 'GET',
                url: 'http://localhost:7373/add',
                qs: {a: '5', b: '10'},                
            };
            await a_request(options).then((res) => {
                console.log({message: res.body})
                expect(res.body).to.equal('15');
            }).catch((res) => {
                console.log({res});
                expect(true).to.equal(false, `add function failed`);
            });
        });
    });
    
    // check if any test fails and close the server if they do
    afterEach(() => {
        server.close();
    });
});